# README #

You can see the basic color palette here: http://paletton.com/palette.php?uid=43n0u0kqqtdgqCRlsvftAo3x3j4

This Library includes:

- Animated Modals
- Animated Links
- Animated Buttons
- Interactive & Animated Gallery
- A Slew of Utility Classes
- Login styles for modal
- Header breadcrumbs
- Profile Image in Nav
- And More!

Feel free to make any suggestions or report any issues you find here!

How-Tos will be coming soon I hope.

Please Note: I am still converting hard coded sizes to variable based sizes so that is not included (yet).